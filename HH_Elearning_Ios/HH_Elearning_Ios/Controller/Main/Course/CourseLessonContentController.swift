//
//  CourseLessonContentController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/25/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Reachability
import Toast_Swift
import SwiftyJSON
import MBProgressHUD
import Alamofire
import YoutubePlayer_in_WKWebView

class CourseLessonContentController: UIViewController {
    
    @IBOutlet weak var viewYoutubePlayer: WKYTPlayerView!
    @IBOutlet weak var tblTest: UITableView!
    var videoType = Int()
    var lessonListId = Int()
    var lessonListName = String()
    var videoUrl = String()
    var youtubeVideoUrl = String()
    var TestListData: [dataLessonTestList] = []
    var testList:[String] = []
    var reachabitily:Reachability?
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    @IBOutlet weak var videoView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getTestList()
        print(youtubeVideoUrl)
        print(videoUrl)
        if videoType == 1 {
            viewYoutubePlayer.load(withVideoId: youtubeVideoUrl)
            videoView.isHidden = true
            viewYoutubePlayer.isHidden = false

        }else{
            videoView.isHidden = false
            viewYoutubePlayer.isHidden = true
            setupVideo()
        }
//        print("video: \(videoUrl)")
        // Do any additional setup after loading the view.
        
    }
//    func extractYoutubeId(fromLink link: String) -> String {
//        let regexString: String = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
//        let regExp = try? NSRegularExpression(pattern: regexString, options: .caseInsensitive)
//        let array: [Any] = (regExp?.matches(in: link, options: [], range: NSRange(location: 0, length: (link.count ))))!
//        if array.count > 0 {
//            let result: NSTextCheckingResult? = array.first as? NSTextCheckingResult
//            return (link as NSString).substring(with: (result?.range)!)
//        }
//
//        return ""
//    }
    func setupVideo(){
        let url = URL(string: videoUrl)
        if url != nil {
            print("url ở đây")
            print(URL(string: videoUrl)!)
            self.player = AVPlayer(url: URL(string: videoUrl)!)
            self.avpController = AVPlayerViewController()
            self.avpController.player = self.player
            avpController.view.frame = videoView.bounds
            self.addChild(avpController)
            self.videoView.addSubview(avpController.view)
            avpController.player?.play()
        }else{
            self.view.makeToast("Hiện không có video", duration: 3.0)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = lessonListName
    }
    override func viewWillDisappear(_ animated: Bool) {
        avpController.player?.pause()
        self.title = " "
    }
}
extension CourseLessonContentController: UITableViewDelegate, UITableViewDataSource {
    func tableSetup(){
        tblTest.tableFooterView = UIView(frame: .zero)
        tblTest.backgroundColor = .white
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.testList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestListCell", for: indexPath) as! TestListCell
        cell.testName.text = self.TestListData[indexPath.row].nameoftest
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CourseTestController") as! CourseTestController
        newViewController.TestContentId = self.TestListData[indexPath.row].id
        newViewController.ContentdataId = self.lessonListId
        newViewController.counter = self.TestListData[indexPath.row].time
        newViewController.originCounter = self.TestListData[indexPath.row].time
//        if indexPath.row == 0 {
//            newViewController.pdfUrl = "http://113.190.235.80/pdf/DATT_Nguyen_Duc_Lai_13_06_2020.pdf"
//        }else{
            newViewController.pdfUrl = self.TestListData[indexPath.row].pdf
//        }
        self.navigationController?.pushViewController(newViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension CourseLessonContentController {
    func getTestList(){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
                
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let catContentData: Parameters=[
                "id": lessonListId
            ]
                    
            let encodeCContentURL = apiGetTest
            let requestToAPI = AF.request(encodeCContentURL, method: .post, parameters: catContentData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        if code == 200{
                            let json = JSON(payload)
                            json["data"].array?.forEach({(testListData) in
                                let testListData = dataLessonTestList(id: testListData["id"].intValue,
                                                                      nameoftest: testListData["nameoftest"].stringValue,
                                                                      time: testListData["time"].intValue,
                                                                      pdf: testListData["pdf"].stringValue )
                                self.TestListData.append(testListData)
                                self.testList.append(testListData.nameoftest)
                                })
                            self.tblTest.reloadData()
                        }else{
                            self.view.makeToast("\(message)", duration: 3.0)
                        }
                    }
                case .failure(let error):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(error)
                        self.view.makeToast(ConnectionTimeout, duration: 3.0)
                    }
                })
                }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(NoInternetConnection, duration: 3.0)
            }
    }
}
