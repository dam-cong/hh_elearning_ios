//
//  CouseLessonListController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/25/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import Alamofire
import Reachability
import SwiftyJSON
import Toast_Swift
import MBProgressHUD

class CouseLessonListController: UIViewController {

    @IBOutlet weak var colLessonCategory: UICollectionView!
    @IBOutlet weak var tblLessonList: UITableView!
    var lessonId:Int!
    var lessonName = String()
    var reachabitily:Reachability?
    var selectedArray = [String]()
    var selectedIndex = 0
    var selectedIndexPath = IndexPath(item: 0, section: 0)
    var queue: [String] = []
    var magicArray:[String] = []
    var LessonListData: [dataLessonList] = []
    var LessonCategoryData: [dataLessonCategory] = []
    var LessonTestListData: [dataLessonTestList] = []
    var TestListData: [dataLessonTestList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegate()
        TopMenuData()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.title = lessonName
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = " "
    }
}
extension CouseLessonListController{
    func setupDelegate(){
         tblLessonList.tableFooterView = UIView(frame: .zero)
        tblLessonList.backgroundColor = .white
    }
    func refreshContent() {
        tblLessonList.reloadData()
    }
}
extension CouseLessonListController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return queue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LessonListCell", for: indexPath) as! LessonListCell
        cell.lblLessonListName.text = queue[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if LessonListData.indices.contains(indexPath.row) {
           let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
           let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CourseLessonContentController") as! CourseLessonContentController
           newViewController.lessonListId = self.LessonCategoryData[selectedIndex].id
            newViewController.lessonListName = self.LessonCategoryData[selectedIndex].namecoursecontent
           //        newViewController.lessonListId = self.LessonListData[indexPath.row].id
            
           newViewController.videoUrl = self.LessonListData[indexPath.row].urlvideo
            newViewController.youtubeVideoUrl = self.LessonListData[indexPath.row].urlVideoYoutube
            newViewController.videoType = self.LessonListData[indexPath.row].type
           self.navigationController?.pushViewController(newViewController, animated: true)
           tableView.deselectRow(at: indexPath, animated: true)
        } else{
//            getTestList(lessonListId: self.LessonCategoryData[selectedIndex].id)
//            print(self.TestListData[indexPath.row].id)
//            print(self.TestListData[indexPath.row].id)

            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CourseTestController") as! CourseTestController
            newViewController.TestContentId = self.TestListData[indexPath.row].id
            newViewController.ContentdataId = self.LessonCategoryData[selectedIndex].id
            newViewController.counter = self.TestListData[indexPath.row].time
            newViewController.originCounter = self.TestListData[indexPath.row].time
            newViewController.pdfUrl = self.TestListData[indexPath.row].pdf
            self.navigationController?.pushViewController(newViewController, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}
extension CouseLessonListController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.magicArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let menuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LessonCategoryCell", for: indexPath) as! LessonCategoryCell
        menuCell.setupCell(text: self.LessonCategoryData[indexPath.item].namecoursecontent)
        menuCell.layer.borderColor = UIColor(red: 0/255, green: 153/255, blue: 153/255, alpha: 1).cgColor
        menuCell.layer.borderWidth = 1
        menuCell.layer.cornerRadius = 10
        return menuCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width / CGFloat(3), height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        queue.removeAll()
        LessonListData.removeAll()
        TestListData.removeAll()
        LessonListData(self.LessonCategoryData[indexPath.item].id)
//        if queue.count == 0 {
//            print("oke")
//        }
        refreshContent()
    }
}

extension CouseLessonListController{
    func getTestList(lessonListId: Int){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
                
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let catContentData: Parameters=[
                "id": lessonListId
            ]
                    
            let encodeCContentURL = apiGetTest
            let requestToAPI = AF.request(encodeCContentURL, method: .post, parameters: catContentData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        if code == 200{
                            let json = JSON(payload)
                            json["data"].array?.forEach({(testListData) in
                                let testListData = dataLessonTestList(id: testListData["id"].intValue,
                                                                      nameoftest: testListData["nameoftest"].stringValue,
                                                                      time: testListData["time"].intValue,
                                                                      pdf: testListData["pdf"].stringValue)
                                self.TestListData.append(testListData)
                                })
                            print(self.TestListData)
                        }else{
                            self.view.makeToast("\(message)", duration: 3.0)
                        }
                    }
                case .failure(let error):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(error)
                        self.view.makeToast(ConnectionTimeout, duration: 3.0)
                    }
                })
                }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(NoInternetConnection, duration: 3.0)
            }
    }
}
extension CouseLessonListController {
    func getLessonTestList(_ CategoryId: Int){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }

        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let catContentData: Parameters=[
                "id": CategoryId
            ]

            let encodeCContentURL = apiGetTest
            let requestToAPI = AF.request(encodeCContentURL, method: .post, parameters: catContentData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)

            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        if code == 200{
                            let json = JSON(payload)
                            json["data"].array?.forEach({(testListData) in
                                let testListData = dataLessonTestList(id: testListData["id"].intValue,
                                                                      nameoftest: testListData["nameoftest"].stringValue,
                                                                      time: testListData["time"].intValue,
                                                                      pdf: testListData["pdf"].stringValue)
                                self.TestListData.append(testListData)
                                self.queue.append(testListData.nameoftest)

                                })
                            print(self.queue.count)
                            print(self.TestListData)
                            self.tblLessonList.reloadData()
                        }else{
                            self.view.makeToast("\(message)", duration: 3.0)
                        }
                    }
                case .failure(let error):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(error)
                        self.view.makeToast(ConnectionTimeout, duration: 3.0)
                    }
                })
                }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(NoInternetConnection, duration: 3.0)
            }
    }
    func TopMenuData(){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
                
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let catContentData: Parameters=[
                "id": lessonId!
            ]
                    
            let encodeCContentURL = apiCourseContentData
            let requestToAPI = AF.request(encodeCContentURL, method: .post, parameters: catContentData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        if code == 200{
                            let json = JSON(payload)
                            json["data"].array?.forEach({(lessonCategoryData) in
                                let lessonCategoryData = dataLessonCategory(id: lessonCategoryData["id"].intValue ,
                                                                            namecoursecontent: lessonCategoryData["namecoursecontent"].stringValue,
                                                                            coursecontentid: lessonCategoryData["coursecontentid"].intValue,
                                                                            type: lessonCategoryData["type"].intValue,
                                                                            urlyoutube: lessonCategoryData["urlyoutube"].stringValue)
                                self.LessonCategoryData.append(lessonCategoryData)
                                self.magicArray.append(lessonCategoryData.namecoursecontent)
                                })
                            self.colLessonCategory.reloadData()
                            self.colLessonCategory.selectItem(at: self.selectedIndexPath, animated: false, scrollPosition: .centeredVertically)
                            self.LessonListData(self.LessonCategoryData[0].id)
                        }else{
                            self.view.makeToast("\(message)", duration: 3.0)
                        }
                    }
                case .failure(let error):
//                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(error)
//                        self.view.makeToast(ConnectionTimeout, duration: 3.0)
                    }
                })
                }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(NoInternetConnection, duration: 3.0)
            }
        }
    func LessonListData(_ CategoryId: Int){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print("Sai lắm bạn ơi :(, thế này là nát rồi")
        }
                
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let catContentData: Parameters=[
                "id": CategoryId
            ]
                    
            let encodeLessonData = apiLessonListData
            let requestToAPI = AF.request(encodeLessonData, method: .post, parameters: catContentData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        if code == 200{
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let json = JSON(payload)
                        json["data"].array?.forEach({(lessonListData) in
                            let lessonListData = dataLessonList(id: lessonListData["id"].intValue ,
                                                                namevideo: lessonListData["name_video"].stringValue,
                                                                urlvideo: lessonListData["urlvideo"].stringValue,
                                                                urlVideoYoutube: lessonListData["urlVideoYoutube"].stringValue,
                                                                coursecontentdataid: lessonListData["coursecontentdataid"].intValue,
                                                                type: lessonListData["type"].intValue)
                            self.LessonListData.append(lessonListData)
                            self.queue.append(lessonListData.namevideo)
                            })
                            print("đây")
                            print(self.LessonListData)
                            self.tblLessonList.reloadData()
                        }else{
                            MBProgressHUD.hide(for: self.view, animated: true)
//                            self.view.makeToast("\(message)", duration: 3.0)
                            self.getLessonTestList(CategoryId)
                            print(message)
                        }
                    }
                case .failure(let error):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(error)
                        self.view.makeToast("Mất kết nối với máy chủ", duration: 3.0)
                    }
                })
                }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast("Vui lòng kiểm tra lại kết nối mạng", duration: 3.0)
            }
        }
}
