//
//  CourseTestResultController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/25/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import Reachability
import Toast_Swift

class CourseTestResultController: UIViewController {
    var reachabitily:Reachability?

    @IBOutlet weak var tblResult: UITableView!
//    let idUser = UserDefaults.standard.string(forKey: "User_id") ?? "Unknown user"
    var idContentdata = Int()
    var idTest = Int()
    var resultAnswerArray = [String]()
    var resultQuestionArray = [Int]()
    var receiveDataContent: [DataTestResult] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupResultTable()
        getTestResult()
        self.navigationItem.setHidesBackButton(true, animated: true);

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = " "
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = " "
    }

    @IBAction func btnRetakeTapped(_ sender: Any) {
//        self.navigationController?.popViewController(animated: false)
        self.popViewControllerss(popViews: 2)
    }
    func popViewControllerss(popViews: Int, animated: Bool = true) {
        if self.navigationController!.viewControllers.count > popViews
        {
            let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
             self.navigationController?.popToViewController(vc, animated: animated)
        }
    }
}
extension CourseTestResultController: UITableViewDelegate, UITableViewDataSource{
    func setupResultTable(){
        tblResult.tableFooterView = UIView(frame: .zero)
        tblResult.backgroundColor = .white
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.receiveDataContent.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestResultCell") as! TestResultCell
        cell.lblQuestion.text = self.receiveDataContent[indexPath.row].question
        cell.lblAnswerA.text = self.receiveDataContent[indexPath.row].a
        cell.lblAnswerB.text = self.receiveDataContent[indexPath.row].b
        cell.lblAnswerC.text = self.receiveDataContent[indexPath.row].c
        cell.lblAnswerD.text = self.receiveDataContent[indexPath.row].d
        cell.lblCorrectAnswer.text = self.receiveDataContent[indexPath.row].correctanswer
        cell.lblExplanation.text = self.receiveDataContent[indexPath.row].feedback
        
        addHeight(label: cell.lblQuestion, const: cell.lblQuestionHeight)
        addHeight(label: cell.lblAnswerA, const: cell.lblAnswerAHeight)
        addHeight(label: cell.lblAnswerB, const: cell.lblAnswerBHeight)
        addHeight(label: cell.lblAnswerC, const: cell.lblAnswerCHeight)
        addHeight(label: cell.lblAnswerD, const: cell.lblAnswerDHeight)
//        addHeight(label: cell.lblCorrectAnswer, const: cell.lblCorrectAnswerHeight)
        addHeight(label: cell.lblExplanation, const: cell.lblExplanationHeight)


        checkFurigana(Answer: self.receiveDataContent[indexPath.row].question, label: cell.lblQuestion)
        checkFurigana(Answer: self.receiveDataContent[indexPath.row].a, label: cell.lblAnswerA)
        checkFurigana(Answer: self.receiveDataContent[indexPath.row].b, label: cell.lblAnswerB)
        checkFurigana(Answer: self.receiveDataContent[indexPath.row].c, label: cell.lblAnswerC)
        checkFurigana(Answer: self.receiveDataContent[indexPath.row].d, label: cell.lblAnswerD)
        checkFurigana(Answer: self.receiveDataContent[indexPath.row].correctanswer, label: cell.lblCorrectAnswer)
        checkFurigana(Answer: self.receiveDataContent[indexPath.row].feedback, label: cell.lblExplanation)
        
        return cell
    }
    
    func addHeight(label: UILabel, const: NSLayoutConstraint) {
        if label.calculateMaxLines() > 1 {
            const.constant = CGFloat(label.calculateMaxLines()*30)
        }else{
            const.constant = 30
        }
    }
    
    func checkFurigana(Answer: String, label: RubyLabel) {
        if !Answer.contains("<") {
            label.isUseRuby = false
        }else{
            label.isUseRuby = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension CourseTestResultController {
    func getTestResult(){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
                
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let catContentData: Parameters=[
                "userid": UserDefaults.standard.string(forKey: "User_id") ?? "Unknown user",
                "contentdataid": idContentdata,
                "id": idTest,
                "idanswer": resultQuestionArray,
                "answer": resultAnswerArray
            ]
                    
            let encodeTestResult = apiSubmitAnswerData
            let requestToAPI = AF.request(encodeTestResult, method: .post, parameters: catContentData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        let mark = resultValue["diem"] as! Int
                        if code == 200{
                            let json = JSON(payload)
                            print(json)
                            json["data"].array?.forEach({(TestResult) in
                                let TestResult = DataTestResult(id: TestResult["id"].intValue,
                                                                question: TestResult["question"].stringValue,
                                                                a:"A. " + TestResult["a"].stringValue,
                                                                b:"B. " + TestResult["b"].stringValue,
                                                                c:"C. " + TestResult["c"].stringValue,
                                                                d:"D. " + TestResult["d"].stringValue,
                                                                correctanswer: "Đáp án đúng: " + TestResult["correctanswer"].stringValue,
                                                                feedback: "Giải thích: " + TestResult["feedback"].stringValue,
                                                                isCorrect: TestResult["correctanswer"].boolValue)
                                self.receiveDataContent.append(TestResult)
                                })
                            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = mainstoryboard.instantiateViewController(withIdentifier: "PopupController") as? PopupController else { return }
//                            vc.image = image
                            
                            vc.idkImTired = UserDefaults.standard.string(forKey: "User_name") ?? "Bạn đạt được"
                            vc.TooTiredForMe = String(mark) + " điểm!"
                            vc.modalPresentationStyle = .overFullScreen
                            vc.modalTransitionStyle = .crossDissolve
                            self.present(vc, animated: true, completion: nil)
                            self.tblResult.reloadData()
                        }else{
                            self.view.makeToast("\(message)", duration: 3.0)
                        }
                    }
                case .failure(let error):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(error)
                        self.view.makeToast(ConnectionTimeout, duration: 3.0)
                    }
                })
                }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(NoInternetConnection, duration: 3.0)
            }
    }
}
