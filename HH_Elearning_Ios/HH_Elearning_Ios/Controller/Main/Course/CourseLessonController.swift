//
//  CourseLessonController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/25/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import Reachability
import Toast_Swift
import Alamofire
import MBProgressHUD
import SwiftyJSON
import Reachability

class CourseLessonController: UIViewController {
    var reachabitily:Reachability?
    
    var DataLesson: [dataLesson] = []
    var lessonId = Int()
    var lessonName = String()
    
    @IBOutlet weak var tableLesson: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataFlow()
        getLessonData()
        // Do any additional setup after loading the view.
        print(UserDefaults.standard.string(forKey: "User_id"))
    }
}
extension CourseLessonController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataLesson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! LessonCell
        cell.lessonName.text = self.DataLesson[indexPath.row].lessonname
        return cell

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CouseLessonListController") as! CouseLessonListController
        newViewController.lessonId = self.DataLesson[indexPath.row].id
        newViewController.lessonName = self.DataLesson[indexPath.row].lessonname
        self.navigationController?.pushViewController(newViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func setupDataFlow(){
//        tableLesson.delegate = self
//        tableLesson.dataSource = self
        tableLesson.tableFooterView = UIView(frame: .zero)
        tableLesson.backgroundColor = .white
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationItem.title = " "
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = lessonName
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
}
extension CourseLessonController {
    func getLessonData(){
            do{
                self.reachabitily = try Reachability.init()
            } catch{
                print(Unreachable)
            }
                if ((reachabitily!.connection) != .unavailable) {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    let catData: Parameters=[
                        "id": lessonId
                    ]
                    let encodeCatURL = apiCoursecontent
                    let requestToAPI = AF.request(encodeCatURL, method: .post, parameters: catData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
                    requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                        switch response.result{
                        case .success(let payload):
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let json = JSON(payload)
                            json["data"].array?.forEach({(catData) in
                                let catData = dataLesson(id: catData["id"].intValue ,
                                                         lessonname: catData["lessonname"].stringValue)
                                self.DataLesson.append(catData)
                             })
                            self.tableLesson.reloadData()
                        case .failure(let error):
                            MBProgressHUD.hide(for: self.view, animated: true)
                            print(error)
                            self.view.makeToast(ConnectionTimeout, duration: 3.0)
                        }
                    })
                }else{
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast(NoInternetConnection, duration: 3.0)
                }
    }
}
