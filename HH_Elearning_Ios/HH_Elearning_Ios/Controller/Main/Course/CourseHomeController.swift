//
//  CourseHomeController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/25/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import MBProgressHUD
import SwiftyJSON
import Reachability

class CourseHomeController: UIViewController {
    var timerTest : Timer?

    let userId = UserDefaults.standard.integer(forKey: "User_id")
    let courseListCellId = "CourseListCell"
    var dataLevelContent: [DataLevelContent] = []
    var dataKhoaHocJoined: [DataLevelContent] = []
    var reachabitily:Reachability?
    @IBOutlet weak var tblLevelCourse: UITableView!
    var dataIdLevelContent: [Int] = []
    var dataIdKhoaHocJoined: [Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        getLessonData()
        getKhoaHocJoined()
        getUserInfo()
        tblLevelCourse.tableFooterView = UIView(frame: .zero)
        tblLevelCourse.backgroundColor = .white
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated: false)
        navigationController?.navigationBar.barTintColor = UIColor(red: 93/255.0, green: 188/255.0, blue: 210/255.0, alpha: 1.0)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.sendTime(url: apiChecktimeout, inOrOut: "timein")
        
        timerTest = Timer.scheduledTimer(timeInterval: 300.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    @objc func updateCounter(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.sendTime(url: apiChecktimeout, inOrOut: "timeout")
    }
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Học lộ trình bài bản"
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = " "
    }

}
extension CourseHomeController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataLevelContent.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: courseListCellId ) as! CourseListCell
        cell.lblNameCourse.text = dataLevelContent[indexPath.row].levelcourse
        cell.idKhoaHoc = dataLevelContent[indexPath.row].id
        
        if dataIdKhoaHocJoined.contains(dataLevelContent[indexPath.row].id) {
            cell.btnJoinKhoaHoc.isHidden = true
        }
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataIdKhoaHocJoined.contains(dataLevelContent[indexPath.row].id) {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CourseLessonController") as! CourseLessonController
            newViewController.lessonId = dataLevelContent[indexPath.row].id
            newViewController.lessonName = dataLevelContent[indexPath.row].levelcourse
            self.navigationController?.pushViewController(newViewController, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        }else{
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}
extension CourseHomeController: CourseListCellDelegate{
    func didTapBtnJoinKhoaHoc(id: Int) {
        joinKhoaHoc(id: userId, lvid: id)
    }
}
extension CourseHomeController {
    func registerCell() {
        tblLevelCourse.register(UINib.init(nibName: courseListCellId, bundle: nil), forCellReuseIdentifier: courseListCellId)
    }
    func getLessonData(){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
            if ((reachabitily!.connection) != .unavailable) {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                let catData: Parameters=[
                    "id": 2
                ]
                let encodeURL = apiGetLevelContent
                let requestToAPI = AF.request(encodeURL, method: .post, parameters: catData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
                requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                    switch response.result{
                    case .success(let payload):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let json = JSON(payload)
                            
                        json["data"].array?.forEach({(Data) in
                            let Data = DataLevelContent(id: Data["id"].intValue, levelcourse: Data["levelcourse"].stringValue)
                                self.dataLevelContent.append(Data)
                            self.dataIdLevelContent.append(Data.id)
                        })
                        self.tblLevelCourse.reloadData()
                    case .failure(let error):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(error)
                        self.view.makeToast(ConnectionTimeout, duration: 3.0)
                        }
                })
            }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(NoInternetConnection, duration: 3.0)
            }
    }
    func joinKhoaHoc(id: Int, lvid: Int) {
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let Data: Parameters=[
                "userid": id,
                "lvid": lvid,
                "status": 1
            ]
            let encodeURL = apiPutStatus
            let requestToAPI = AF.request(encodeURL, method: .post, parameters: Data, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.getKhoaHocJoined()
                case .failure(let error):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print(error)
                    self.view.makeToast(ConnectionTimeout, duration: 3.0)
                }
            })
        }else{
            MBProgressHUD.hide(for: self.view, animated: true)
            self.view.makeToast(NoInternetConnection, duration: 3.0)
        }
    }
    func getKhoaHocJoined() {
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let Data: Parameters=[
                "userid": userId
            ]
            let encodeURL = apiLevelStatus
            let requestToAPI = AF.request(encodeURL, method: .post, parameters: Data, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    let json = JSON(payload)
                    json["data"].array?.forEach({(Data) in
                        let Data = DataLevelContent(id: Data["id"].intValue, levelcourse: Data["levelcourse"].stringValue)
                            self.dataKhoaHocJoined.append(Data)
                        self.dataIdKhoaHocJoined.append(Data.id)
                    })
                    
                    self.tblLevelCourse.reloadData()
                case .failure(let error):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print(error)
                    self.view.makeToast(ConnectionTimeout, duration: 3.0)
                }
            })
        }else{
            MBProgressHUD.hide(for: self.view, animated: true)
            self.view.makeToast(NoInternetConnection, duration: 3.0)
        }
    }
    func getUserInfo() {
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let Data: Parameters=[
                "userid": UserDefaults.standard.integer(forKey: "User_id")
            ]
            let encodeURL = apiGetUserInfo
            let requestToAPI = AF.request(encodeURL, method: .post, parameters: Data, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    let json = JSON(payload)
//                    json["data"].array?.forEach({(Data) in
//                        let Data = DataLevelContent(id: Data["id"].intValue, levelcourse: Data["levelcourse"].stringValue)
//                            self.dataKhoaHocJoined.append(Data)
//                        self.dataIdKhoaHocJoined.append(Data.id)
//                    })
                    let code = json["status_code"].intValue
                    if code == 200 {
                        let data = json["data"]
                        let name = data["name"]
//                        print(name)
                        UserDefaults.standard.set(name.stringValue, forKey: "User_name")

                    }else{
                        UserDefaults.standard.set(false, forKey: "ISUSERLOGGEDIN")
                        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
                        self.navigationController?.pushViewController(newViewController, animated: true)
                    }
                case .failure(let error):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print(error)
                    self.view.makeToast(ConnectionTimeout, duration: 3.0)
                }
            })
        }else{
            MBProgressHUD.hide(for: self.view, animated: true)
            self.view.makeToast(NoInternetConnection, duration: 3.0)
        }
    }
}
