//
//  CourseTestController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/25/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import Reachability
import Toast_Swift
import PDFKit

class CourseTestController: UIViewController {
    var reachabitily:Reachability?
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var pdfViewHeightConst: NSLayoutConstraint!
    
    var sectionHeaderViewHeight:CGFloat = 10.0
    var counter = Int()
    var originCounter = Int()
    var timerTest : Timer?
    var ContentdataId = Int()
    var TestContentId = Int()
    var testDataContent: [dataLessonTestContent] = []
    var answerArray = [String]()
    var questionArray = [String]()
    var selectedQuestionArray = [Int]()
    var selectAnswerArr = [String]()
    var pdfUrl = String()
    @IBOutlet weak var tblTest: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTestTable()
        getTestData()
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        if pdfUrl == "" {
            pdfViewHeightConst.constant = 0
            pdfView.isHidden = true
        }else{
            pdfView.isHidden = false
            pdfViewHeightConst.constant = (screenHeight * 4)/10
            setupPDFView()
        }
        print(pdfUrl)
        print("pdf đây nè")
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.tblTest.reloadData()
    }
    @IBAction func btnSendAnswerTapped(_ sender: Any) {
        counter = originCounter
        timerTest?.invalidate()
        timerTest = nil
        SendAnswer()
    }
    //http://113.190.235.80/pdf/DATT_Nguyen_Duc_Lai_13_06_2020.pdf
    func setupPDFView(){
        if let pdfDocument = PDFDocument(url: URL(string: pdfUrl)!) {
            pdfView.displayMode = .singlePageContinuous
            pdfView.autoScales = true
            pdfView.displayDirection = .vertical
            pdfView.document = pdfDocument
        }
    }
    
    func SendAnswer(){
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CourseTestResultController") as! CourseTestResultController
        newViewController.idContentdata = self.ContentdataId
        newViewController.idTest = self.TestContentId
        newViewController.resultQuestionArray.append(contentsOf: selectedQuestionArray)
        newViewController.resultAnswerArray.append(contentsOf: selectAnswerArr)
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        counter = 0
        self.title = " "
    }
    
    override func viewWillAppear(_ animated: Bool) {
        counter = originCounter
        let minutes = String(counter / 60)
        let seconds = String(counter % 60)
        self.title = minutes + ":" + seconds
        setupTimer()
    }
}
extension CourseTestController {
    func setupTimer() {
        timerTest = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        RunLoop.current.add(timerTest!, forMode: RunLoop.Mode.common)
    }
    @objc func updateCounter() {
        if counter > 0 {
            counter -= 1
            let minutes = String(counter / 60)
            let seconds = String(counter % 60)
            self.title = minutes + ":" + seconds
        }else{
            SendAnswer()
            counter = originCounter
            let minutes = String(counter / 60)
            let seconds = String(counter % 60)
            self.title = minutes + ":" + seconds
            timerTest?.invalidate()
            timerTest = nil
        }
    }
}
extension CourseTestController: UITableViewDataSource, UITableViewDelegate{

    func setupTestTable(){
        tblTest.tableFooterView = UIView(frame: .zero)
        tblTest.layoutIfNeeded()
        self.tblTest.isEditing = true
        self.tblTest.allowsMultipleSelectionDuringEditing = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.testDataContent[section].answer.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestContentCell", for: indexPath) as! TestContentCell
        if indexPath.row == 0 {
            cell.lblAnswer.text = "Câu \(indexPath.section + 1): " + self.testDataContent[indexPath.section].question
            cell.lblAnswer.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 204/255.0, alpha: 1.0)
            if cell.lblAnswer.calculateMaxLines() > 1 {
                cell.lblAnswerHeight.constant = CGFloat(cell.lblAnswer.calculateMaxLines()*30)
            }else{
                cell.lblAnswerHeight.constant = 30
            }
        }else{
            cell.lblAnswer.text = self.testDataContent[indexPath.section].answer[indexPath.row - 1]
            cell.lblAnswer.textColor = .black
            if cell.lblAnswer.calculateMaxLines() > 1 {
                cell.lblAnswerHeight.constant = CGFloat(cell.lblAnswer.calculateMaxLines()*30)
            }else{
                cell.lblAnswerHeight.constant = 30
            }
        }
        
        return cell
    }
//    self.testDataContent[section].question
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.testDataContent.count
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.indexPathsForSelectedRows?
            .filter { $0.section == indexPath.section }
            .forEach { tableView.deselectRow(at: $0, animated: true) }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 19
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let result = UIView()

        // recreate insets from existing ones in the table view
        let insets = tableView.separatorInset
        let width = tableView.bounds.width - insets.left - insets.right
        let sepFrame = CGRect(x: insets.left, y: -0.5, width: width, height: 0.5)

        // create layer with separator, setting color
        let sep = CALayer()
        sep.frame = sepFrame
        sep.backgroundColor = tableView.separatorColor?.cgColor
        result.layer.addSublayer(sep)

        return result
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            tableView.deselectRow(at: indexPath, animated: true)
        }else{
            self.selectDeselecCell(tableView: tblTest, indexPath: indexPath)
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.selectDeselecCell(tableView: tblTest, indexPath: indexPath)
    }
    func checkFurigana(Answer: String, label: RubyLabel) {
        if !Answer.contains("<") {
            label.isUseRuby = false
        }else{
            label.isUseRuby = true
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 {
            return false
        }else{
            return true
        }
    }
}


// MARK: TACH
extension UIView {
    func bindEdgesToSuperview() {
        guard let s = superview else {
            preconditionFailure("`superview` nil in bindEdgesToSuperview")
        }
        translatesAutoresizingMaskIntoConstraints = false

        leadingAnchor.constraint(equalTo: s.leadingAnchor, constant: 10).isActive = true
        trailingAnchor.constraint(equalTo: s.trailingAnchor, constant: -10).isActive = true
        topAnchor.constraint(equalTo: s.topAnchor, constant: 10).isActive = true
        bottomAnchor.constraint(equalTo: s.bottomAnchor, constant: -10).isActive = true
    }
}
extension CourseTestController {
    func selectDeselecCell(tableView: UITableView, indexPath: IndexPath){
        self.selectAnswerArr.removeAll()
        self.selectedQuestionArray.removeAll()
        if let arr = tblTest.indexPathsForSelectedRows  {
            for index in arr {
                switch index.row {
                case 0:
                    selectAnswerArr.append("sai :(")
                case 1:
                    selectAnswerArr.append("a")
                case 2:
                    selectAnswerArr.append("b")
                case 3:
                    selectAnswerArr.append("c")
                case 4:
                    selectAnswerArr.append("d")
                case 5:
                    selectAnswerArr.append("e")
                default:
                    selectAnswerArr.append("f")
                }
                selectedQuestionArray.append(testDataContent[index.section].id)
            }
        }
    }
    func getTestData(){
            do{
                self.reachabitily = try Reachability.init()
            } catch{
                print(Unreachable)
            }
                
            if((reachabitily!.connection) != .unavailable) {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                let data: Parameters=[
                    "id": TestContentId
                    ]
                
                let encodeTestData = apiGetRandomQuestion
                let requestToAPI = AF.request(encodeTestData, method: .post, parameters: data, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
                requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                        
                    switch response.result{
                    case .success(let payload):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let json = JSON(payload)
                        json["data"].array?.forEach({(testData) in
                            let testData = dataLessonTestContent(id: testData["id"].intValue ,
                                                                 question: testData["question"].stringValue,
                                                                 answer: [testData["a"].stringValue,
                                                                          testData["b"].stringValue,
                                                                          testData["c"].stringValue,
                                                                          testData["d"].stringValue,
                                                                          testData["e"].stringValue,
                                                                          testData["f"].stringValue,
                                                                          testData["g"].stringValue].filter({ $0 != ""}),
                                                                 correctanswer: testData["correctanswer"].stringValue)
                            self.testDataContent.append(testData)
                            self.answerArray.append(testData.correctanswer)
                            self.questionArray.append(String(testData.id))
                        })
                        print(self.testDataContent)
                        DispatchQueue.main.async {
                            self.tblTest.reloadData()
                        }
                    case .failure(let error):
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.makeToast(ConnectionTimeout, duration: 3.0)
                        print(error)
                    }
                })
            }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(NoInternetConnection, duration: 3.0)
            }
    }
}
