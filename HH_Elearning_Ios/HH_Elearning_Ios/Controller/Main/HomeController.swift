//
//  HomeController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/25/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit

class HomeController: UIViewController {

    @IBOutlet weak var btnCamNhanHocVien: UIButton!
    @IBOutlet weak var btnVanHoaNhatBan: UIButton!
    @IBOutlet weak var btnLuyenThi: UIButton!
    @IBOutlet weak var btnHocLTBB: UIButton!
    @IBOutlet weak var btnKtraDauVao: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        DesignHome(viewCornerRadius: 10)
        navigationController?.navigationBar.barTintColor = UIColor(red: 93/255.0, green: 188/255.0, blue: 210/255.0, alpha: 1.0)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Home"
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title = " "
    }
    func DesignHome(viewCornerRadius: CGFloat) {
        btnCamNhanHocVien.layer.cornerRadius = viewCornerRadius
        btnVanHoaNhatBan.layer.cornerRadius = viewCornerRadius
        btnLuyenThi.layer.cornerRadius = viewCornerRadius
        btnHocLTBB.layer.cornerRadius = viewCornerRadius
        btnKtraDauVao.layer.cornerRadius = viewCornerRadius
    }

}
