//
//  SpeechToTextController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 6/15/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import InstantSearchVoiceOverlay

class SpeechToTextController: UIViewController,VoiceOverlayDelegate {
    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblMark: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var startStopBtn: UIButton!
    let jpArray = ["おはようございます", //Ohayōgozaimasu
                   "こんにちは", //Kon'nichiwa
                   "おやすみなさい", //Oyasuminasai
                   "さようなら", //Sayōnara
                   "すみません", //Sumimasen
                   "おにいちゃん", // 2-chan
                   "おねがいします", //Onegaishimasu
                   "はじめましょう", //Hajimemashou
                   "はい、わかります", //Hai, wakarimasu
                   "いいえ、わかりません", //Īe, wakarimasen
                   "もういちど", //Mō ichido
                   "さあ、どうぞ", //Sā, dōzo
                   "もういい", //Mō ī,
                   "何 てこと", //Nanite koto
                   "おまえは もう 死んでいる"] //Omae wa mō shinde iru
    var currentWord = String()
    lazy var voiceOverlayController: VoiceOverlayController = {
      let recordableHandler = {
        return SpeechController(locale: Locale(identifier: "ja"))
      }
      return VoiceOverlayController(speechControllerHandler: recordableHandler)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentWord = jpArray.randomElement()!
        lblQuestion.text = "Hãy nói: " + currentWord
//        let result = "khong co viec gi kho"
//        let comparable = "おはよう おにいちゃん"
//        print(Tools.levenshtein(aStr: result, bStr: comparable))
//
//        print(result.count)
//        let a = Float(result.count)
//        let b = Float(Tools.levenshtein(aStr: result, bStr: comparable))
//        print(b/a * 100)
//
        
        voiceOverlayController.delegate = self
        
        // If you want to start recording as soon as modal view pops up, change to true
        voiceOverlayController.settings.autoStart = true
        voiceOverlayController.settings.autoStop = true
        voiceOverlayController.settings.showResultScreen = false
    }
    @IBAction func btnSwitch(_ sender: Any) {
        currentWord = jpArray.randomElement()!
        lblQuestion.text = "Hãy nói: " + currentWord
    }
    
    @IBAction func startStopAct(_ sender: Any) {
        voiceOverlayController.start(on: self, textHandler: { (text, final, extraInfo) in
          print("callback: getting \(String(describing: text))")
          print("callback: is it final? \(String(describing: final))")
          
          if final {
            // here can process the result to post in a result screen
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { (_) in
              let myString = text
              let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.red ]
              let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
              
              self.voiceOverlayController.settings.resultScreenText = myAttrString
              self.voiceOverlayController.settings.layout.resultScreen.titleProcessed = "BLA BLA"
            })
          }
        }, errorHandler: { (error) in
          print("callback: error \(String(describing: error))")
        }, resultScreenHandler: { (text) in
          print("Result Screen: \(text)")
        }
        )
    }
    func recording(text: String?, final: Bool?, error: Error?) {
      if let error = error {
        print("delegate: error \(error)")
      }
      
      if error == nil {
        textView.text = text

        let result = currentWord
        let RealResult = result.replace(pattern: "、", template: "")
//        print(result)
//        print(RealResult)
        let dis = RealResult.distance(between: text!)
        let percentage = dis * 100
        lblMark.text = "Tỉ lệ đúng: \(percentage)%"


      }
    }
}
