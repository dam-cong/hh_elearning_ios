//
//  RegisterController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/23/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Reachability
import Toast_Swift

class RegisterController: UIViewController {
    var reachabitily:Reachability?

    @IBOutlet weak var txtPasswordConfirm: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var viewPasswordConfirm: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewName: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        DesignRegister(viewCornerRadius: 10, viewBorderWidth: 1, viewBorderColor: UIColor.systemPurple.cgColor)
        setDelegate()
        hideKeyboard()
        navigationController?.navigationBar.barTintColor = .systemPurple
        // Do any additional setup after loading the view.
    }
    func DesignRegister(viewCornerRadius: CGFloat, viewBorderWidth: CGFloat, viewBorderColor: CGColor) {
        viewName.layer.cornerRadius = viewCornerRadius
        viewName.layer.borderWidth = viewBorderWidth
        viewName.layer.borderColor = viewBorderColor
        
        viewEmail.layer.cornerRadius = viewCornerRadius
        viewEmail.layer.borderWidth = viewBorderWidth
        viewEmail.layer.borderColor = viewBorderColor
        
        viewPassword.layer.cornerRadius = viewCornerRadius
        viewPassword.layer.borderWidth = viewBorderWidth
        viewPassword.layer.borderColor = viewBorderColor
        
        viewPasswordConfirm.layer.cornerRadius = viewCornerRadius
        viewPasswordConfirm.layer.borderWidth = viewBorderWidth
        viewPasswordConfirm.layer.borderColor = viewBorderColor

        btnRegister.layer.cornerRadius = viewCornerRadius
    }
    
    @IBAction func btnRegisterTapped(_ sender: Any) {
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
        insertRegisterData()
    }
    
    func hideKeyboard() {
        let Tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(Tap)
    }
    @objc func DismissKeyboard() {
        view.endEditing(true)
    }


}
extension RegisterController: UITextFieldDelegate{
    func setDelegate(){
        txtName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtPasswordConfirm.delegate = self
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
        viewPassword.resignFirstResponder()
        viewPasswordConfirm.resignFirstResponder()
        return true
    }
}
extension RegisterController{
    func insertRegisterData(){
        if((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let registerData: Parameters=[
                "name":txtName.text!,
                "email":txtEmail.text!,
                "password":txtPassword.text!,
                "password_confirmation":txtPasswordConfirm.text!
            ]
            
            let encodeRegisterURL = apiRegister
            let requestToAPI = AF.request(encodeRegisterURL, method: .post, parameters: registerData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
            
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        if code == 200{
                            self.navigationController?.popViewController(animated: true)
                            
                        }else{
                            self.view.makeToast("\(message)", duration: 3.0)
                        }

                    }
                case .failure(let error):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast(ConnectionTimeout, duration: 3.0)
                    print(error)
                }
                
            })
        }else{
            self.view.makeToast(NoInternetConnection, duration: 3.0)
        }
    }
}
