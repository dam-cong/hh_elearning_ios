//
//  LogoutController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/26/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit

class LogoutController: UIViewController {

    @IBOutlet weak var btnLogout: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.string(forKey: "User_name") == "Hash tài khoản đấy đừng xoá" {
            btnLogout.setTitle("Đăng nhập", for: .normal)
        } else {
            btnLogout.setTitle("Đăng xuất", for: .normal)
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func talkToMeBtnTapped(_ sender: Any) {
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "SpeechToTextController") as! SpeechToTextController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @IBAction func btnLogoutTapped(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "ISUSERLOGGEDIN")
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
}
