//
//  LoginController.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/23/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import Reachability
import Toast_Swift
import Alamofire
import MBProgressHUD
import SwiftyJSON

class LoginController: UIViewController {
    
    var reachabitily:Reachability?

    @IBOutlet weak var btnMeme: UIButton!
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        DesignLogin(viewCornerRadius: 10, viewBorderWidth: 1, viewBorderColor: UIColor.systemPurple.cgColor)
        setDelegate()
        IsAuthorized()
        hideKeyboard()
    }
    
    func hideKeyboard() {
        let Tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(Tap)
    }
    @objc func DismissKeyboard() {
        view.endEditing(true)
    }
  
    @IBAction func btnLoginTapped(_ sender: Any) {
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
        AuthorizeLogin(email: txtEmail.text!, pass: txtPassword.text!)
    }
    @IBAction func btnMemeTapped(_ sender: Any) {
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
        AuthorizeLogin(email: "hashtaikhoandaydungxoa@gmail.com", pass: "123456789")
    }
    func DesignLogin(viewCornerRadius: CGFloat, viewBorderWidth: CGFloat, viewBorderColor: CGColor) {
        EmailView.layer.cornerRadius = viewCornerRadius
        EmailView.layer.borderWidth = viewBorderWidth
        EmailView.layer.borderColor = viewBorderColor
        
        PasswordView.layer.cornerRadius = viewCornerRadius
        PasswordView.layer.borderWidth = viewBorderWidth
        PasswordView.layer.borderColor = viewBorderColor
        
        btnLogin.layer.cornerRadius = viewCornerRadius
        btnMeme.layer.cornerRadius = viewCornerRadius
    }
    func IsAuthorized(){
        if UserDefaults.standard.bool(forKey: "ISUSERLOGGEDIN") == true {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CourseHomeController") as! CourseHomeController
            self.navigationController?.pushViewController(newViewController, animated: false)
        }
    }
    // MARK: - Navigation bar state
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
extension LoginController: UITextFieldDelegate{
    func setDelegate(){
        txtEmail.delegate = self
        txtPassword.delegate = self
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
        return true
    }
}
extension LoginController {
    
    func AuthorizeLogin(email: String, pass: String){
        if ((reachabitily!.connection) != .unavailable) {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let loginData: Parameters=[
                "email":email,
                "password":pass
            ]
            
            let encodeLoginURL = apiLogin
            let requestToAPI = AF.request(encodeLoginURL, method: .post, parameters: loginData, encoding: JSONEncoding.default, headers: nil, interceptor: nil)

            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    UserDefaults.standard.set(true, forKey: "ISUSERLOGGEDIN")
                    
                    if let x = payload as? Dictionary<String,AnyObject>
                    {
                        let resultValue = x as NSDictionary
                        let message = resultValue["message"] as! String
                        let code = resultValue["status_code"] as! Int
                        if code == 200{
                            let json = JSON(payload)
                            json["data"].array?.forEach({(UserData) in
                                let UserData = dataUser(id: UserData["id"].intValue, name: UserData["name"].stringValue, email: UserData["email"].stringValue)
                                UserDefaults.standard.set(UserData.id, forKey: "User_id")
                                UserDefaults.standard.set(UserData.name, forKey: "User_name")
                                UserDefaults.standard.set(UserData.email, forKey: "Email")
                            })
                            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let newViewController = mainstoryboard.instantiateViewController(withIdentifier: "CourseHomeController") as! CourseHomeController
                            self.navigationController?.pushViewController(newViewController, animated: true)
                        }else{
                            self.view.makeToast("\(message)", duration: 3.0)
                        }

                    }
                case .failure(let error):
                    print(error)
                    MBProgressHUD.hide(for: self.view, animated: true)

                    self.view.makeToast(ConnectionTimeout, duration: 3.0)
                }
                
            })
        }else{
            MBProgressHUD.hide(for: self.view, animated: true)
            self.view.makeToast(NoInternetConnection, duration: 3.0)
        }
    }
}
