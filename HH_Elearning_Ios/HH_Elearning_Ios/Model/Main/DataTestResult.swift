//
//  DataTestResult.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/26/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import Foundation
struct DataTestResult {
    var id: Int
    var question: String
    var a: String
    var b: String
    var c: String
    var d: String
    var correctanswer: String
    var feedback: String
    var isCorrect: Bool
}
