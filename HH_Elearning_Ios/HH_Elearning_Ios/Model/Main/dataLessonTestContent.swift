//
//  dataLessonTestContent.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/26/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import Foundation
struct dataLessonTestContent {
    var id: Int
    var question: String
    var answer: [String]
    var correctanswer: String
}
