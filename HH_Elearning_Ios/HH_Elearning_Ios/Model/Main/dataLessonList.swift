//
//  dataLessonList.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/26/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import Foundation
struct dataLessonList {
    var id: Int
    var namevideo: String
    var urlvideo: String
    var urlVideoYoutube: String
    var coursecontentdataid: Int
    var type: Int
}
