//
//  CourseListCell.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/26/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
protocol CourseListCellDelegate {
    func didTapBtnJoinKhoaHoc(id: Int)
}
class CourseListCell: UITableViewCell {
    var idKhoaHoc = Int()
    var delegate: CourseListCellDelegate?

    @IBOutlet weak var btnJoinKhoaHoc: UIButton!
    @IBOutlet weak var lblNameCourse: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnJoinKhoaHocTapped(_ sender: Any) {
        delegate!.didTapBtnJoinKhoaHoc(id: idKhoaHoc)
    }
    
}
