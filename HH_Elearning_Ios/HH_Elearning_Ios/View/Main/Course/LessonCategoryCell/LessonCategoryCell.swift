//
//  LessonCategoryCell.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/26/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit

class LessonCategoryCell: UICollectionViewCell {
    @IBOutlet weak var lblLessonCategory: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblLessonCategory.alpha = 0.6
    }
    
    func setupCell(text: String) {
        lblLessonCategory.text = text
    }
    override var isSelected: Bool{
        didSet{
            lblLessonCategory.alpha = isSelected ? 1.0 : 0.6
        }
    }
}
