//
//  TestResultCell.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/26/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit

class TestResultCell: UITableViewCell {

    @IBOutlet weak var lblExplanationHeight: NSLayoutConstraint!
    @IBOutlet weak var lblAnswerDHeight: NSLayoutConstraint!
    @IBOutlet weak var lblAnswerCHeight: NSLayoutConstraint!
    @IBOutlet weak var lblAnswerBHeight: NSLayoutConstraint!
    @IBOutlet weak var lblAnswerAHeight: NSLayoutConstraint!
    @IBOutlet weak var lblQuestionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCorrectAnswer: RubyLabel!
    @IBOutlet weak var lblAnswerD: RubyLabel!
    @IBOutlet weak var lblAnswerC: RubyLabel!
    @IBOutlet weak var lblAnswerB: RubyLabel!
    @IBOutlet weak var lblAnswerA: RubyLabel!
    @IBOutlet weak var lblExplanation: RubyLabel!
    @IBOutlet weak var lblQuestion: RubyLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
