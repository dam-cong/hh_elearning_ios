//
//  QuestionCell.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 5/27/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit

class QuestionCell: UITableViewCell {
    @IBOutlet weak var lblQuestion: RubyLabel!
    @IBOutlet weak var viewQuestionHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
