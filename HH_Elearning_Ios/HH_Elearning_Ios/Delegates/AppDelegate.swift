//
//  AppDelegate.swift
//  HH_Elearning_Ios
//
//  Created by ERM on 4/23/20.
//  Copyright © 2020 ERM. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Toast_Swift
import Alamofire
import SwiftyJSON
import Reachability

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var reachabitily:Reachability?
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = UIColor(red: 93/255.0, green: 188/255.0, blue: 210/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        IQKeyboardManager.shared.enable = true
//        print(String.random())
        whatsTheCode = String.random()
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack
    @available(iOS 13.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "HH_Elearning_Ios")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    
    @available(iOS 13.0, *)
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    func sendTime(url: String, inOrOut: String){
        do{
            self.reachabitily = try Reachability.init()
        } catch{
            print(Unreachable)
        }
        if ((reachabitily!.connection) != .unavailable) {
            let Data: Parameters=[
                inOrOut: Int(Date().timeIntervalSince1970),
                "userid": UserDefaults.standard.integer(forKey: "User_id"),
                "codeauto": whatsTheCode
            ]
            print(Data)
            let encodeURL = url
            let requestToAPI = AF.request(encodeURL, method: .post, parameters: Data, encoding: JSONEncoding.default, headers: nil, interceptor: nil)
                    
            requestToAPI.responseJSON(completionHandler: {(response) -> Void in
                switch response.result{
                case .success(let payload):
                    print(payload)
                case .failure(let error):
                    print(error)
                }
            })
        }else{
            print(NoInternetConnection)
        }
    }
}

